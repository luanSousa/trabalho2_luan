<?php
    use PHPUnit\Framework\TestCase;

    require_once(dirname(__FILE__, 3).'/classes/Student.class.php');

    class studentIntegracaoTest extends TestCase{
        protected $estudanteObj;

        protected function setUp(): void {
            $this->estudanteObj = new Student();
        }

        public function testIntegradoCreate()
        {
            $this->estudanteObj->setName("Luan");
            $this->estudanteObj->setEmail("luan.sousa@teste.com");
            $retorno = $this->estudanteObj->create();
            $this->assertTrue($retorno, "Erro ao validar asserção");
        }

        public function testIntegracaoDeleteErro()
        {
            $this->estudanteObj->setId(-1);
            $resultado = $this->estudanteObj->delete();
            $this->assertFalse(false, $resultado);
        }



    }



?>