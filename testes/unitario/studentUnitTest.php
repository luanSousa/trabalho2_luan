<?php
    use PHPUnit\Framework\TestCase;

    require_once(dirname(__FILE__, 3).'/classes/Student.class.php');

    class studentUnitTest extends TestCase{
        protected $estudanteObj;

        protected function setUp(): void {
            $this->estudanteObj = new Student();
        }

        public function testSetName()
        {
            $this->estudanteObj->setName("Luan");
            $this->assertEquals("Luan",  $this->estudanteObj->getName());
        }
        public function testSetEmail()
        {
            $this->estudanteObj->setEmail("luan.sousa@teste.com");
            $this->assertEquals("luan.sousa@teste.com",  $this->estudanteObj->getEmail());
        }
        public function testVazio(){
            $this->assertEmpty($this->estudanteObj->setEmail(''));
            $this->assertEmpty($this->estudanteObj->setName(''));
        }



    }



?>